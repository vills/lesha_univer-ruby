#!/usr/bin/env ruby
#coding: UTF-8

require 'tk'

$time = nil
$buttons = nil

def init(window)
	colors = ['red', 'blue', 'black', 'cyan']
	colors = colors.shuffle + colors.shuffle
	$colors = colors.shuffle

	$current = [nil, nil]

	$time = Time.now.to_i

	$buttons = Array.new

	2.times { |i| 
		4.times { |k|
			$buttons << TkButton.new(window) {
				command { bclick(i*4+k) }
				grid('row'=>i+2, 'column'=>k)
				activebackground "white"
				bg "white"
				height 6
				width 13
			}
		}
	}
end


def win(c=0)
	$buttons.each { |i| c=1 if i.bg == "white" }
	return false if c==1
	return true
end

def congrats
	Time.now.to_i - $time
end


def breturn(b)

	if b[0].bg != b[1].bg
		b.each { |i|
			i.bg = "white"
			i.state = "normal"
		}
	end

	$current = [nil, nil]
end


def bclick(index)
	b = $buttons[index]
	b.state = "disabled"
	b.bg = $colors[index]

	if $current[0].nil?
		$current[0] = index+1 
	elsif $current[1].nil?
		$current[1] = index+1
	else
		breturn [ $buttons[$current[0]-1], $buttons[$current[1]-1] ]
		$current[0] = index+1
	end

	if win
		Tk::messageBox :message => "Игра завершена! Вы справились за #{congrats} секунд."
		puts "Игра завершена! Вы справились за #{congrats} секунд."
	end
end


$window = TkRoot.new do
	title "Угадайка"
	minsize(400, 200)
end

TkButton.new($window) {
	text "Новая игра"
	command { init($window) }
	grid('row'=>0, 'column'=>0, 'columnspan'=>4)
	activebackground "white"
	bg "white"
	height 2
	width 63
}


Tk.mainloop